.PHONY: codestyle

codestyle: loganalysis.py
	@python3 -m flake8 $^
